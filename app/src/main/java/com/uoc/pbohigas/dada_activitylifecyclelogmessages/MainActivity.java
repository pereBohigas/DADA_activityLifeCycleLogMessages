package com.uoc.pbohigas.dada_activitylifecyclelogmessages;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

public class MainActivity extends Activity {

    StringBuilder builder = new StringBuilder();
    TextView textView;

    private void log(String text) {
        Log.d("LifeCycleText", text);
        builder.append(text);
        builder.append('\n');
        textView.setText(builder.toString());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        textView = new TextView(this);
        textView.setText(builder.toString());
        setContentView(textView);
        log("created");
    }

    @Override
    protected void onStart(){
        super.onStart();
        log("started");
    }

    @Override
    protected void onResume(){
        super.onResume();
        log("resumed");
    }

    @Override
    protected void onPause(){
        super.onPause();
        log("paused");
        if (isFinishing()) {
            log("finishing");
        }
    }

    @Override
    protected void onStop(){
        super.onStop();
        log("stopped");
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        log("destroyed");
    }
}
